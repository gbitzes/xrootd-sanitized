#!/usr/bin/env bash
set -e
set -x

REPO="$PWD/repo"
rm -rf "$REPO"

mkdir "$REPO"
mkdir "$REPO/srpm"
mkdir "$REPO/rpm"

rm -rf "$PWD/xrootd"
git clone https://github.com/xrootd/xrootd

pushd xrootd
TARGET_TAG="v4.9.0"
git checkout ${TARGET_TAG}

if [[ "$1" == "asan" ]]; then
  # git apply ../asan-patch

  # For CentOS7 require libasan3
  if ! hash dnf 2>/dev/null; then
      sed -i 's/libasan/libasan3/g' ./packaging/rhel/xrootd.spec.in
  fi
elif [[ "$1" == "tsan" ]]; then
  git apply ../tsan-patch
  git -c user.name="Georgios Bitzes" -c user.email="georgios.bitzes@cern.ch" commit --amend -a --no-edit
  git tag -d "${TARGET_TAG}"
  git -c user.name="Georgios Bitzes" -c user.email="georgios.bitzes@cern.ch" tag -a "${TARGET_TAG}" -m "Super legit TSAN tag"
fi

cd packaging
./makesrpm.sh --output $REPO/srpm
popd

# For CentOS7 still use yum commands
if ! hash dnf 2>/dev/null; then
  yum-builddep -y $REPO/srpm/*.src.rpm
else
  dnf builddep -y $REPO/srpm/*.src.rpm
fi

if [[ "$1" == "asan" ]]; then
  rpmbuild --rebuild $REPO/srpm/*.src.rpm --define '_with_asan 1'
elif [[ "$1" == "tsan" ]]; then
  rpmbuild --rebuild $REPO/srpm/*.src.rpm --define '_with_tsan 1'
else
  rpmbuild --rebuild $REPO/srpm/*.src.rpm
fi

tree ~/

find ~/rpmbuild/RPMS -name '*.rpm' -exec cp {} $REPO/rpm \;
